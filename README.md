#ecs040 -- notes
- - -

## Table of Contents
1. Introduction
2. Dependencies
3. Installation
4. Development

- - -

## 1. Introduction 
ecs040 is a repository which holds notes to aid in learning C++ and 
object-oriented programming

## 2. Dependencies

### General

* [latex](http://www.latex-project.org)

## 3. Usage


### To build

	pdflatex ecs040.tex

### To clean generated files

	rake clean

### To clean ALL generated files (including PDF)

	rake clobber

## 4. Development
* Author: 					[Martin Velez](http://www.martinvelez.com)
* Copyright: 				Copyright (C) 2012 [Martin Velez](http://www.martinvelez.com)
* License: 					[GPL](http://www.gnu.org/copyleft/gpl.html)

### Source 
[Bitbucket](https://bitbucket.org/martinvelez/ecs040) is hosting this code.
	
	https://bitbucket.org/martinvelez/ecs040

### Issues
Provide feedback, get help, request features, and report bugs here:

	https://bitbucket.org/martinvelez/ecs040/issues

